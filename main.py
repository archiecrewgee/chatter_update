from loguru import logger as log
import numpy as np
import random

# Constants #
dataPackets = 100 

class Node:
    data = np.zeros(dataPackets)
    listeningOffset = random.randint(0, dataPackets - 1)    # listen 1% of time
    dropRate = 0.01

    def listening(self, time: int):
        return (time % dataPackets) == self.listeningOffset
    
    
class Transmitter:
    def transmitting(self, time):
        return True        # always transmitting

class World:
    nodes: Node = []
    time: int = 0

    def step(self):
        time += 1
        log.info(f"Stepping to time: {time}")

        for node in self.nodes:
            if node.listening(time) and self.transmitter.transmitting() and random.random() > node.dropRate:
                node.data[self.transmitter.current_packet_index()] = True
# build nodes
nodes = []
nodes.append(Node())

# build transmitter
transmitter = Transmitter()

log.info("Hello world")