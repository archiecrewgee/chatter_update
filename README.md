# Chatter Update 

This project simulates the transfer of information between theoretical energy constrained IoT devices. These devices are constrained such that they can only transcieve information occasionally. Within the project these devices will be refered to as `nodes`. The goal is to observe how the environment affects the complete knowledge by all nodes

The data-to-transmit can be broken up into packets consisting of some arbitary unit such that ${data} = N * {packet}$. THe packet size is considered to be exclusively the data frame to simplify abstraction. Packets must each contain a probabilistic drop rate for their transmission; this may be dependant on input variables.

Nodes are characterised as follows:

 * `transmitting function`: describes the behaviour for 'deciding' to transmit a given packet of data or additional telegram.
 * `recieving function`: describes a region of time where the node may recieve transmitted packets 
 * TODO: fill this shit in... placement in space

## Setup

